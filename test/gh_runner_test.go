package test

import (
	"fmt"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/random"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"

	"golang.org/x/crypto/ssh"

	"github.com/gruntwork-io/terratest/modules/terraform"
)

const fixtureFolder = "../examples/ghrunner"
const tfvarfile = "gh_runner_test.tfvars"

func teardown(t *testing.T, ff string) {
	terraformOptions := test_structure.LoadTerraformOptions(t, ff)
	terraform.Destroy(t, terraformOptions)
}
func TestGhRunner(t *testing.T) {
    // Teardown on completion or early fail or fatal exit
	defer test_structure.RunTestStage(t, "cleanup_terraform", func() {
		teardown(t,fixtureFolder)
	})

	// Run tests in parallel
	t.Parallel()

	// Certain resoruces, like keyvault, have special naming conventions, like "The name must begin with a letter, end with a letter or digit, and not contain consecutive hyphens"
	uniquePostfix := fmt.Sprintf("test-%s", random.UniqueId())
	// Construct the terraform options with default retryable errors to handle the most common
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// Set the path to the Terraform code that will be tested.
		TerraformDir: "../examples/ghrunner",
		// Unique postfix for the resource to be created by the test
		Vars: map[string]interface{}{
			"postfix": uniquePostfix,
		},
		VarFiles: []string{tfvarfile},
	})

	// Use Terratest to deploy the infrastructure
	test_structure.RunTestStage(t, "setup", func() {
		// Save options for later test stages
		test_structure.SaveTerraformOptions(t, fixtureFolder, terraformOptions)

		// Triggers the terraform init and terraform apply command
		if _, err :=terraform.InitAndApplyE(t, terraformOptions); err != nil {
			defer teardown(t,fixtureFolder)
			t.Fatalf("Setup failed with err: %v, cleaning up...",err)
		}
	})

	test_structure.RunTestStage(t, "validate", func() {
		// run validation checks here
		terraformOptions := test_structure.LoadTerraformOptions(t, fixtureFolder)

		vmPubIPAddr := terraform.Output(t, terraformOptions, "vm_public_ip")

		// it takes some time for Azure to assign the public IP address so it's not available in Terraform output after the first apply
		attemptsCount := 0
		for vmPubIPAddr == "" && attemptsCount < 5 {
			// add wait time to let Azure assign the public IP address and apply the configuration again, to refresh state.
			time.Sleep(30 * time.Second)
			terraform.Apply(t, terraformOptions)
			vmPubIPAddr = terraform.Output(t, terraformOptions, "vm_public_ip")
			attemptsCount++
		}

		if vmPubIPAddr == "" {
			t.Fatal("Cannot retrieve the public IP address value for the linux VM.")
		}
		sshKey := terraform.Output(t, terraformOptions, "tls_private_key")

		signer, err := ssh.ParsePrivateKey([]byte(sshKey))
		if err != nil {
			t.Fatalf("Unable to parse private key: %v", err)
		}

		sshConfig := &ssh.ClientConfig{
			// Method bases filepath on tf apply execution folder
			User: terraform.GetVariableAsStringFromVarFile(t,fmt.Sprintf("%s/%s",fixtureFolder,tfvarfile),"admin_user"),
			Auth: []ssh.AuthMethod{
				ssh.PublicKeys(signer),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		}

		sshConnection, err := ssh.Dial("tcp", fmt.Sprintf("%s:22", vmPubIPAddr), sshConfig)
		if err != nil {
			t.Fatalf("Cannot establish SSH connection to vm-linux-1 public IP address: %v", err)
		}

		sshSession, err := sshConnection.NewSession()
		if err != nil {
			t.Fatalf("Cannot create SSH session to vm-linux-1 public IP address: %v", err)
		}

		defer func(sshSession *ssh.Session) {
			err := sshSession.Close()
			if err != nil {
				t.Fatalf("Couldn't close ssh connection")
			}
		}(sshSession)
	})


}
