module cheesuscrust89/tfaz/m/v2

go 1.16

require (
	github.com/gruntwork-io/terratest v0.37.11
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
