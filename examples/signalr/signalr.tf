# Signalr resource group
resource "azurerm_resource_group" "signalr" {
  name     = "${var.signalr_name}-rs"
  location = var.signalr_location
}


module "signalr" {
  source = "../../modules/signalr"

  signalr_sku            = var.signalr_sku
  signalr_instance_count = var.signalr_instance_count
  signalr_service_mode   = var.signalr_service_mode
  signalr_name           = var.signalr_name
  signalr_rsg            = azurerm_resource_group.signalr.name
  signalr_location       = var.signalr_location

}
