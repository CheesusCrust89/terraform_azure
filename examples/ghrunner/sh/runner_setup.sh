#!/bin/bash
set -euxo pipefail

# Download the latest runner package
mkdir /home/${admin_user}/actions-runner && cd /home/${admin_user}/actions-runner || exit
echo "Downloading runner from URL: ${runner_url}"
curl -o ${filename} -L ${runner_url}
echo "Runner downloaded."
# Unpack
tar xzf ./${filename}
# Install dependencies and redirect to outputfile
echo "Installing dependencies"
./bin/installdependencies.sh &> dep_out
# Enable the flag to run as root since the the CSE agent on the VM runs as root, create the runner, start config and redirect to out
echo "Running config setup"
RUNNER_ALLOW_RUNASROOT="1" ./config.sh --url https://github.com/${gh_org}/${gh_repo} --token ${gh_token} --unattended &> config_out
# Setting up actions as a service: https://docs.github.com/en/actions/hosting-your-own-runners/configuring-the-self-hosted-runner-application-as-a-service
# Install runner
./svc.sh install &> install_out
echo "SVC installed."
# Start the runner
./svc.sh start
# Needed so that the CSE completes
exit 0