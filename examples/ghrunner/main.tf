terraform {
  # This module is now only being tested with Terraform 0.13.x. However, to make upgrading easier, we are setting
  # 0.12.26 as the minimum version, as that version added support for required_providers with source URLs, making it
  # forwards compatible with 0.13.x code.
  required_version = ">= 1.0.0"
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.15.1"
    }
  }
}

provider "azurerm" {
  features {}
  # settings this explicitly here to not rely on az login automagic account values when applying
  subscription_id = var.subscription_id
}

locals {
  runner_subnet_name = "runner-subnet"
  # Runner related local variables for download and setup
  filename="actions-runner-linux-x64-${var.runner_version}.0.tar.gz"
  runner_url="https://github.com/actions/runner/releases/download/v${var.runner_version}/actions-runner-linux-x64-${var.runner_version}.tar.gz"
  tags = {
    environment = "dev"
    team = "cloudinfra"
  }
}

# Data of current tenant and subscription
data "azurerm_subscription" "current" {
}

data "azurerm_client_config" current {
}

# RG for the gh runner
resource "azurerm_resource_group" "gh_runner_rg" {
  name     =  format("%s", var.postfix)
  location = var.location
  tags = local.tags
}

# Networking
module "gh_runner_vnet" {
  source              = "../../modules/vnet"
  resource_group_name = azurerm_resource_group.gh_runner_rg.name
  location            = var.location
  vnet_name           = format("%s-vnet",var.postfix)
  address_space       = ["10.0.0.0/16"]
  subnets = [
    {
      name : local.runner_subnet_name
      address_prefixes : ["10.0.1.0/24"]
    }
  ]
}

# Create public IPs
resource "azurerm_public_ip" "runner_vm_publicip" {
  name                         = format("%s-publicip",var.postfix)
  location                     = var.location
  resource_group_name          = azurerm_resource_group.gh_runner_rg.name
  allocation_method            = "Static"

  tags = local.tags
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "gh_runner_nsg" {
  name                = format("%s-nsg",var.postfix)
  location            = var.location
  resource_group_name = azurerm_resource_group.gh_runner_rg.name

  # Allow ssh on the VM
  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = local.tags
}

# Create network interface
resource "azurerm_network_interface" "gh_runner_nic" {
  name                      = format("%s-nic",var.postfix)
  location                  = var.location
  resource_group_name       = azurerm_resource_group.gh_runner_rg.name

  ip_configuration {
    name                          = format("%s-nic-config",var.postfix)
    subnet_id                     = module.gh_runner_vnet.subnet_ids[local.runner_subnet_name]
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.runner_vm_publicip.id
  }

  tags = local.tags
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "gh_runner_assoc" {
  network_interface_id      = azurerm_network_interface.gh_runner_nic.id
  network_security_group_id = azurerm_network_security_group.gh_runner_nsg.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.gh_runner_rg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics. Naming is like this because Azure.
resource "azurerm_storage_account" "ghrunnerstorageaccount" {
  name                        = "diag${random_id.randomId.hex}"
  resource_group_name         = azurerm_resource_group.gh_runner_rg.name
  location                    = var.location
  account_tier                = "Standard"
  account_replication_type    = "LRS"

  tags = local.tags
}

# Identity for the VM
resource "azurerm_user_assigned_identity" "gh_runner_vm_uai" {
  resource_group_name = azurerm_resource_group.gh_runner_rg.name
  location            = var.location
  name                = format("%s-uai",var.postfix)
}

# Assign the Vm's managed identity Contributor role for Key-Vault uses
resource "azurerm_role_assignment" "gh_runner_contrib" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "Contributor"
  # SP id from the Vnet's managed identity
  principal_id         =  azurerm_user_assigned_identity.gh_runner_vm_uai.principal_id
}

# Create an SSH key
resource "tls_private_key" "gh_runner_vm_ssh" {
  algorithm = "RSA"
  rsa_bits = 4096
}

# Create the KV
module "gh_runner_kv" {
  source = "../../modules/keyvault"
  location = var.location
  kv_name= format("%s-kv",var.postfix)
  tags = local.tags
  resource_group = azurerm_resource_group.gh_runner_rg.name
  tenant_id = data.azurerm_client_config.current.tenant_id
}

# KV access policy for the deploying infra
resource "azurerm_key_vault_access_policy" "sh_gh_runner_dep_kv_pol" {
  key_vault_id = module.gh_runner_kv.kv_id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  key_permissions = [
    "Create",
    "Get",
  ]

  secret_permissions = [
    "Set",
    "Get",
    "Delete",
    "Purge",
    "Recover"
  ]
}

# Create the secret for the gh token
resource "azurerm_key_vault_secret" "gh_token" {
  depends_on = [azurerm_key_vault_access_policy.sh_gh_runner_dep_kv_pol]
  name         = "gh-token"
  value        = var.gh_token
  key_vault_id = module.gh_runner_kv.kv_id
}

# KV access policy for the VM's UAMI
resource "azurerm_key_vault_access_policy" "vm_uami_kv_pol" {
  key_vault_id = module.gh_runner_kv.kv_id
  tenant_id    = data.azurerm_client_config.current.tenant_id

  /* The actual UUID of the associated service principal for the managed identity for binding the policy. Naming, as usual is confusing,
     but the key difference here is that this object_id refers to the AD id(which every entity has, users service principals etc.),
     which is found under the principal_id field of the resource.
  */
  object_id    = azurerm_user_assigned_identity.gh_runner_vm_uai.principal_id

  key_permissions = [
    "Get","List",
  ]

  secret_permissions = [
    "Get","List",
  ]
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "gh_runner_vm" {
  name                  = format("%s-vm",var.postfix)
  location              = var.location
  resource_group_name   = azurerm_resource_group.gh_runner_rg.name
  network_interface_ids = [azurerm_network_interface.gh_runner_nic.id]
  size                  = "Standard_DS1_v2"
  computer_name  = var.postfix
  admin_username = var.admin_user
  # SSH with keys only
  disable_password_authentication = true

  # User assigned managed identity, see https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/tutorial-linux-vm-access-arm
  identity {
    type = "UserAssigned"
    /* Takes the id string, which is a resource URI string, not the actual UUID for the identity,
      like: "/subscriptions/subid/resourcegroups/rgName/providers/Microsoft.ManagedIdentity/userAssignedIdentities/identityName"
      see: https://docs.microsoft.com/en-us/rest/api/managedidentity/user-assigned-identities/get
    */
    identity_ids = [azurerm_user_assigned_identity.gh_runner_vm_uai.id]
  }

  os_disk {
    name              = "RunnerOsDisk"
    caching           = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  admin_ssh_key {
    username       = var.admin_user
    public_key     = tls_private_key.gh_runner_vm_ssh.public_key_openssh
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.ghrunnerstorageaccount.primary_blob_endpoint
  }

  tags = local.tags
}

/*  CSE for the last mile config for the VM
    The Custom Script Extension runs as root.
*/
resource "azurerm_virtual_machine_extension" "gh_runner_cse" {
  name                 = format("%s-setup",var.postfix)
  virtual_machine_id   = azurerm_linux_virtual_machine.gh_runner_vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"
  /* <!DO NOT USE IN PRODUCTIOM SETTING, POSSIBLE LEAK OF CREDENTIALS!>
      see https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/custom-script-linux#property-script for more details
  */
  settings = <<SETTINGS
    {
        "script": "${base64encode(templatefile("./sh/runner_setup.sh", {
          filename="${local.filename}", runner_url= "${local.runner_url}",gh_org="${var.gh_org}", gh_repo="${var.gh_repo}", gh_token="${var.gh_token}", admin_user ="${var.admin_user}"
        }))}"
    }
SETTINGS

  tags = local.tags
}


