variable "location" {
  description = "The location to set for the resource group."
  type        = string
  default     = "norwayeast"
}

variable "subscription_id" {
  description = "The subscription ID where resources are provisioned"
  type = string
}

variable "postfix" {
  description = "A postfix string to centrally mitigate resource name collisions."
  type        = string
  default     = "resource"
}

variable "admin_user" {
  description = "Name of the azure administrator for the VM. Mandatory to include in config since Makefile reads it from it."
  type        = string
}

variable "runner_version" {
  description = "GH runner version."
  type        = string
  default = "2.281.1"
}

variable "gh_org" {
  description = "GH org needed for runner setup URL."
  type        = string
}

variable "gh_repo" {
  description = "GH repo name needed for runner setup URL."
  type        = string
}

variable "gh_token" {
  description = "GH token that is needed for auth."
  type        = string
  sensitive = true
}
