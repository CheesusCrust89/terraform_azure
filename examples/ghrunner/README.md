# Self-hosted GH runner on a single VM with Terraform 
## Introduction
[This](https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners) is the manual imperative tutorial for this configuration. Have translated this to IaC with Terraform. 
The purpose of this example is to give teams hands-on experience with self-hosted runners if they don't want to spin up a full-blown AKS cluster to host it(Which I would recommend because HA is easier and overall installation with Helm is pretty straightforward).
This is a bare metal installation, no containerization whatsoever for the runner.
When authenticating with github apps, see the [AKS guide](https://github.com/actions-runner-controller/actions-runner-controller#setting-up-authentication-with-github-api) same logic applies to VMs.
## A few gotchas 
* only use this for private repos(creds can leak otherwise)
* when you add a runner to a repo (you can add runners on an organization or an enterprise level as well) you'll need an access token that's autogenerated when you click on the add runner button(see [this](https://github.com/orgs/community/discussions/26751) for details) in the UI. Luckily GH published the API docs, so now this can be done programatically as well, will update the code with something smart later on, for now it's a var.
* **READ THE COMMENTS IN THE CODE** >> chances are you'll find the answers to questions there when deploying. Tried to pay extra attention to highlighting anomalies.
## How to deploy
1. Create the repo on GH, generate the token (this will be automated here in the future) 
2. Set the defined variables in the vars.tf file the way you want to (I personally do it in a `.tfvar` file, but it's up to you)
3. Run `terraform apply`
4. If you want to troubleshoot the VM, ssh into it with `make ssh_vm`. The script is smart, it will get the admin name, the ssh key and the public IP of the VM for you automagically either from the `.tfvars` file or the TF output.
## Running the tests 
Yes, there's actual tests for this (even tho a bit limited, will expand on it in the future). 
The tests are under the root in the `test` folder. Run like you would any go test, but be mindful that it requires it's own `.tfvars` file, defined under `const tfvarfile = "gh_runner_test.tfvars"`. I advise not using the same subscription for testing purposes.
