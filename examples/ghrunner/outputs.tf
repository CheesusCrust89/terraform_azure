output "tls_private_key" {
  value = tls_private_key.gh_runner_vm_ssh.private_key_pem
  # Set to true so that there's no leaking of keys in the pipeline
  sensitive = true
}

output "vm_public_ip" {
  value = azurerm_public_ip.runner_vm_publicip.ip_address
  sensitive = false
}
