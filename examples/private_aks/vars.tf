variable "location" {
  description = "The location to set for the resource group."
  type        = string
  default     = "westeurope"
}
// Bastion admin credentials
variable "bastion_admin_usr" {
  type = string
}

variable "bastion_admin_pwd" {
  description = "Password between 4-30 characters, upercase, numbers and signs."
  type        = string
}

variable "postfix" {
  description = "A postfix string to centrally mitigate resource name collisions."
  type        = string
  default     = "resource"
}

variable "hub_resource_group_name" {
  description = "The resource_group name for the vnet."
  type        = string
}

variable "kube_resource_group_name" {
  description = "The resource_group name for the aks cluster."
  type        = string
}

variable "hub_vnet_name" {
  description = "Hub VNET name."
  default     = "hub-firewall-vnet"
}

variable "kube_vnet_name" {
  description = "AKS VNET name."
  default     = "spoke-kube-vnet"
}