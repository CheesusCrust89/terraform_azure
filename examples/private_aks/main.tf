locals {
  deployment_name = "private-aks"
  aks_subnet_name = "aks-subnet"
  // The Subnet used for the Firewall must have the name AzureFirewallSubnet and the subnet mask must be at least a /26.
  // See:
  // TODO link to firewall docs
  firewall_subnet_name = "AzureFirewallSubnet"
  bastion_subnet_name  = "bastion-subnet"
  // Can only be named AzureBastionSubnet
  // See: https://docs.microsoft.com/en-us/azure/bastion/tutorial-create-host-portal#createhost
  bastion_svc_subnet_name = "AzureBastionSubnet"

  // Cluster specific naming
  cluster_name = "private-aks"
}

terraform {
  required_version = ">= 1.0.0"
}

provider "azurerm" {
  version = "~>3.0.2"
  features {}
}

// Resource groups for the vnet and the aks cluster. Naming convention follows AZ guidelines.
resource "azurerm_resource_group" "hub" {
  name     = format("%s-%s", local.deployment_name, var.hub_resource_group_name)
  location = var.location
}

resource "azurerm_resource_group" "kube" {
  name     = format("%s-%s", local.deployment_name, var.kube_resource_group_name)
  location = var.location
}

// VNet and subnets for the firewall, the bastion server and svc.
// TODO move address spacing to config instead of hardcode
module "hub_network" {
  source              = "../../modules/vnet"
  resource_group_name = azurerm_resource_group.hub.name
  location            = var.location
  vnet_name           = var.hub_vnet_name
  address_space       = ["10.0.0.0/22"]
  subnets = [
    {
      name : local.firewall_subnet_name
      address_prefixes : ["10.0.0.0/26"]
    },
    {
      name : local.bastion_subnet_name
      address_prefixes : ["10.0.1.0/24"]
    },
    {
      name : local.bastion_svc_subnet_name
      address_prefixes : ["10.0.2.0/24"]
    }
  ]
}

// The AKS vnet and the dedicated subnets.
// TODO move address spacing to config instead of hardcode
module "kube_network" {
  source              = "../../modules/vnet"
  resource_group_name = azurerm_resource_group.kube.name
  location            = var.location
  vnet_name           = var.kube_vnet_name
  address_space       = ["10.0.4.0/22"]
  subnets = [
    {
      name : local.aks_subnet_name
      address_prefixes : ["10.0.5.0/24"]
    }
  ]
}

// Peering setup between the cluster and the hub, both directions
// TODO this could be probably cleverly extracted to a module
resource "azurerm_virtual_network_peering" "hub_to_kube" {
  name                      = "HubToKube"
  virtual_network_name      = var.hub_vnet_name
  resource_group_name       = azurerm_resource_group.hub.name
  remote_virtual_network_id = module.kube_network.vnet_id
}

resource "azurerm_virtual_network_peering" "kube_to_hub" {
  name                      = "KubeToHub"
  virtual_network_name      = var.kube_vnet_name
  resource_group_name       = azurerm_resource_group.kube.name
  remote_virtual_network_id = module.hub_network.vnet_id
}

// Firewall and rule settings
module "firewall" {
  source         = "../../modules/firewall"
  resource_group = azurerm_resource_group.hub.name
  location       = var.location
  pip_name       = "azureFirewalls-ip"
  fw_name        = "kubenet-fw"
  subnet_id      = module.hub_network.subnet_ids[local.firewall_subnet_name]
  dns_servers    = ["168.63.129.16"]
}

// Routetable for the cluster
module "route_table" {
  source              = "../../modules/route_table"
  resource_group      = azurerm_resource_group.hub.name
  location            = var.location
  rt_name             = "kubenet_forward_fw_rt"
  route_name          = "kubenet_forward_fw_route"
  firewall_private_ip = module.firewall.fw_private_ip
  subnet_id           = module.kube_network.subnet_ids[local.aks_subnet_name]
}

// The AKS cluster
// Check the default values in the module for node sizes and network config!
module "private_aks" {
  source         = "../../modules/aks"
  cluster_name   = local.cluster_name
  location       = var.location
  resource_group = azurerm_resource_group.kube.name
  aks_subnet_id  = module.kube_network.subnet_ids[local.aks_subnet_name]
  depends_on     = [module.route_table]
}

// Bastion VM and service
module "bastion" {
  source   = "../../modules/bastion"
  location = var.location

  // Bastion VM and SVC settings
  bastion_admin_usr     = var.bastion_admin_usr
  bastion_admin_pwd     = var.bastion_admin_pwd
  bastion_subnet_id     = module.hub_network.subnet_ids[local.bastion_subnet_name]
  bastion_svc_subnet_id = module.hub_network.subnet_ids[local.bastion_svc_subnet_name]

  // Cluster specific values for dns private zone
  cluster_fqdn             = module.private_aks.private_fqdn
  cluster_name             = local.cluster_name
  kube_resource_group_name = azurerm_resource_group.kube.name

  // Hosting vnet values
  hub_resource_group_name = azurerm_resource_group.hub.name
  hub_vnet_id             = module.hub_network.vnet_id

}
