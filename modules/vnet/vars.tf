variable "resource_group_name" {
  description = "Resource Group name."
  type        = string
}

variable "location" {
  description = "Location in which to deploy the network."
  type        = string
}

variable "vnet_name" {
  description = "VNET name."
  type        = string
}

variable "address_space" {
  description = "VNET address space."
  type        = list(string)
}

variable "dns_servers" {
  description = "The DNS servers' IP. Default's to "
  type        = list(any)
  default     = ["168.63.129.16"]
}

// TODO very likely this can use the subnet struct from TF, no need for object like this
variable "subnets" {
  description = "Subnets configuration."
  type = list(object({
    name             = string
    address_prefixes = list(string)
  }))
}

variable "tags" {
  description = "Associated tags with teh resource."
  type        = object({})
  default = {
    env = "dev"
    team = "cloudinfra"
  }
}