// Bastion VM location
variable "location" {
  description = "The location to set for the resource group."
  type        = string
}

// Bastion admin credentials
variable "bastion_admin_usr" {
  type = string
}

variable "bastion_admin_pwd" {
  type = string
}

variable "cluster_fqdn" {
  description = "The FQDN of the private aks cluster"
  type        = string
}

variable "cluster_name" {
  description = "The name of the aks cluster"
  type        = string
}

variable "hub_vnet_id" {
  description = "The ID of the Hub vnet"
  type        = string
}

variable "kube_resource_group_name" {
  description = "The resource_group name for the aks cluster."
  type        = string
}

variable "hub_resource_group_name" {
  description = "The resource_group name for the aks cluster."
  type        = string
}

variable "bastion_subnet_id" {
  description = "Bastion VM subnet ID."
  type        = string
}

variable "bastion_svc_subnet_id" {
  description = "Bastion service subnet ID."
  type        = string
}
