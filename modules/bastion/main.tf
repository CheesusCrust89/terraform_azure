
// Link the Bastion Vnet to the Private DNS Zone generated to resolve the Server IP from the URL in Kubeconfig

resource "azurerm_private_dns_zone_virtual_network_link" "link_bastion_cluster" {
  name = "dnslink-bastion-cluster"
  // The Terraform language does not support user-defined functions, and so only the functions built in to the language are available for use.
  // The below code gets the private dns zone name from the fqdn, by slicing the out dns prefix
  private_dns_zone_name = join(".", slice(split(".", var.cluster_fqdn), 1, length(split(".", var.cluster_fqdn))))
  // The resource group where the Private DNS Zone exists. Magic auto-generated RG for the cluster, follows the below naming pattern.
  resource_group_name = format("MC_%s_%s_%s", var.kube_resource_group_name, var.cluster_name, var.location)
  virtual_network_id  = var.hub_vnet_id
}

resource "azurerm_network_interface" "bastion_nic" {
  name                = "nic-bastion"
  location            = var.location
  resource_group_name = var.hub_resource_group_name
  ip_configuration {
    name                          = "internal"
    subnet_id                     = var.bastion_subnet_id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "aks-bastion" {
  name                            = "aks-bastion"
  location                        = var.location
  resource_group_name             = var.hub_resource_group_name
  size                            = "Standard_D2_v2"
  admin_username                  = var.bastion_admin_usr
  admin_password                  = var.bastion_admin_pwd
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.bastion_nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

// Create an Azure Bastion Service to access the Bastion VM

resource "azurerm_public_ip" "pip_azure_bastion" {
  name                = "pip-azure-bastion"
  location            = var.location
  resource_group_name = var.hub_resource_group_name

  allocation_method = "Static"
  sku               = "Standard"
}

resource "azurerm_bastion_host" "azure-bastion" {
  name = "azure-bastion"
  location = var.location
  resource_group_name = var.hub_resource_group_name
  ip_configuration {
    name = "configuration"
    subnet_id = var.bastion_svc_subnet_id
    public_ip_address_id = azurerm_public_ip.pip_azure_bastion.id
  }
}

  # CSE - execute custom script on VM at creation
  resource "azurerm_virtual_machine_extension" "bastion-cse" {
    name                 = format("%s-bootstrap", azurerm_linux_virtual_machine.aks-bastion.name)
    virtual_machine_id   = azurerm_linux_virtual_machine.aks-bastion.id
    publisher            = "Microsoft.Azure.Extensions"
    type                 = "CustomScript"
    type_handler_version = "2.0"
    protected_settings   = <<SETTINGS
    {
        "script": "${base64encode(file("${path.module}/bootstrap.sh"))}"
    }
SETTINGS
  }