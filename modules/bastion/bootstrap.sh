#! /bin/bash
# Install deps
sudo apt update && sudo apt install -y apt-transport-https gnupg2
# Install Kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo 'deb https://apt.kubernetes.io/ kubernetes-xenial main' | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt update
sudo apt install -y kubectl
###
# Install nano - because VIM makes Baby Jesus cry
sudo apt install -y nano
###
# Install az-cli
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
###
# Install security updates and such
sudo apt upgrade -y