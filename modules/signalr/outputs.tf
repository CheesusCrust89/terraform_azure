output "signalr_endpoint" {
  description = "Signalr service endpoint"
  value       = azurerm_signalr_service.signalr_service.name

}
