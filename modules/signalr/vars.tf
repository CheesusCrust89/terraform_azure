variable "signalr_name" {
  description = "The name of the signalr instance"
  type        = string
}

variable "signalr_rsg" {
  description = "RSG name for signalr"
  type        = string
}

variable "signalr_location" {
  description = "Location of the RSG"
  type        = string
}

variable "signalr_sku" {
  description = " Specifies which tier to use. Valid values are Free_F1 and Standard_S1"
  type        = string
}

variable "signalr_instance_count" {
  description = "No of instances, valid values are 1, 2, 5, 10, 20, 50 and 100"
  type        = number
}

variable "signalr_service_mode" {
  description = "Service mode it can be Default, Serverless, and Classic"
  type        = string
}
