# Signalr service concept details
# https://docs.microsoft.com/en-us/azure/azure-signalr/signalr-concept-internals

resource "azurerm_signalr_service" "signalr_service" {
  name                = var.signalr_name
  location            = var.signalr_location
  resource_group_name = var.signalr_rsg

# Specifies which pricing tier to use. Valid values are Free_F1 and Standard_S1.
# https://portal.azure.com/#create/Microsoft.SignalRGalleryPackage

  sku {
    name     = var.signalr_sku
    capacity = var.signalr_instance_count
  }

# Service mode is an important concept in Azure SignalR Service. Azure SignalR Service currently supports three service modes: default, serverless and classic. Your SignalR resource will behave differently in different modes.
# https://docs.microsoft.com/en-us/azure/azure-signalr/concept-service-mode

  features {
    flag  = "ServiceMode"
    value = var.signalr_service_mode
  }

}
