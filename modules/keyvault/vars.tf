variable "resource_group" {
  description = "Resource group name."
  type        = string
}

variable "location" {
  description = "Location where Firewall will be deployed."
  type        = string
}

variable "kv_name" {
  description = "Name of the keyvault."
  type        = string
}

variable "tenant_id" {
  description = "The tenant id where the keyvault is located"
  type = string
}

variable "tags" {
  description = "List of tags on the resource"
  type = object({})
}