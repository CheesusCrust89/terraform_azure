
resource "azurerm_kubernetes_cluster" "private_aks" {
  name                    = var.cluster_name
  location                = var.location
  kubernetes_version      = var.kube_version
  resource_group_name     = var.resource_group
  dns_prefix              = var.cluster_name

  // Setting to true disables public endpoints for the k8s API
  private_cluster_enabled = true

  default_node_pool {
    name           = "default"
    node_count     = var.nodepool_nodes_count
    vm_size        = var.nodepool_vm_size
    vnet_subnet_id = var.aks_subnet_id
  }

  // For more info on AKS identity management, see: https://docs.microsoft.com/en-us/azure/aks/use-managed-identity
  identity {
    type = "SystemAssigned"
  }

  network_profile {
    docker_bridge_cidr = var.network_docker_bridge_cidr
    dns_service_ip     = var.network_dns_service_ip
    network_plugin     = "azure"
    // Setting this prevents AKS from automatically configuring egress
    // See: https://docs.microsoft.com/en-us/azure/aks/egress-outboundtype#outbound-type-of-userdefinedrouting
    outbound_type = "userDefinedRouting"
    service_cidr  = var.network_service_cidr
  }
}
