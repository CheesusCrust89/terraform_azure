variable "location" {
  description = "The location to set for the aks resource group."
  type        = string
}

variable "cluster_name" {
  description = "The name of the aks cluster"
  type        = string
}

variable "resource_group" {
  description = "The resource_group name for the aks cluster."
  type        = string
}

variable "kube_version" {
  description = "AKS Kubernetes version prefix. Formatted '[Major].[Minor]' like '1.18'. Patch version part (as in '[Major].[Minor].[Patch]') will be set to latest automatically."
  default     = "1.23.3"
}

variable "nodepool_nodes_count" {
  description = "Default nodepool nodes count"
  default     = 1
}

variable "nodepool_vm_size" {
  description = "Default nodepool VM size"
  default     = "Standard_D2_v2"
}

variable "network_docker_bridge_cidr" {
  description = "CNI Docker bridge cidr"
  default     = "172.17.0.1/16"
}

variable "network_dns_service_ip" {
  description = "CNI DNS service IP"
  default     = "10.2.0.10"
}

variable "network_service_cidr" {
  description = "CNI service cidr"
  default     = "10.2.0.0/24"
}

variable "aks_subnet_id" {
  description = "AKS subnet ID."
  type        = string
}