from diagrams import Cluster, Diagram, Edge
from diagrams.azure.compute import AKS, VMClassic
from diagrams.azure.network import Firewall, DNSPrivateZones, PublicIpAddresses, VirtualNetworks
from diagrams.custom import Custom

# Creates the main diagram
# You can use direction to organize the diagram, accepted values are TB, BT, LR, RL
with Diagram(name="Private AKS", show=False, direction= "TB"):  
    internet = Custom("Internet", "./icons/internet.png")
    docker = Custom("Docker Registry", "./icons/docker.png")
# Creates the AKS icons
    with Cluster ("AKS Resource Group"):
        with Cluster ("Spoke AKS VNET"):
            spoke_aks_vnet = VirtualNetworks()
            with Cluster ("AKS Subnet"):
                aks = [AKS("Private AKS")]

# Creates the Hub icons
    with Cluster ("Hub Resource Group"):
        with Cluster("Hub VNET"):
           hub_vnet = VirtualNetworks()
           with Cluster ("Bastion Subent"):
             bastion  = VMClassic("Bastion Host")
           with Cluster ("Azure Firewall Subnet"):
               with Cluster("Firewall"):
                 firewall = Firewall()
                 public_ip = PublicIpAddresses("Public IP")
                 private_ip = PublicIpAddresses("Private IP")
  

# Creates the Private DNS icons
    with Cluster("MC_private Resource Group"):
          private_dns = DNSPrivateZones("Private DNS")
     
# Creats the connection lines between resources
    internet >> Edge(color="brown", style="bold", label="80 - 443") << public_ip >> Edge(color="brown", style="bold") << private_ip >> Edge(color="brown", style="bold") << aks
    docker >> Edge(color="blue", style="bold", label="FQDN Restricted") << public_ip >> Edge(color="blue", style="bold") << private_ip >> Edge(color="blue", style="bold") << aks
    spoke_aks_vnet >> Edge(color="darkgreen", style="dashed, bold", label="Peering") << hub_vnet
    spoke_aks_vnet << private_dns >> hub_vnet
    bastion >> Edge(color="gray", style="dashed, bold") >> aks
    