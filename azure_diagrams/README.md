# Instructions

## 0. The diagrams are created by using the framework: 
https://github.com/mingrammer/diagrams

## 1. Graphviz package is reaquired and needs to be installed:
https://graphviz.gitlab.io/download/

If you use Debian/Ubuntu you can install Graphviz by using the following command:
```
sudo apt install graphviz

```

## 2. Install Poetry: 
https://python-poetry.org/docs/#installation


If you use Debian/Ubuntu you can install Poetry by using the following command:

```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```


## 3. Install diagrams using poetry:

```
poetry add diagrams

```

## 4. Diagrams documentation:
https://diagrams.mingrammer.com/
